package Bean;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Jason on 2/6/16.
 */
public class LeaderBoardData implements Serializable{

    private Map<String, Long> rows = new HashMap<String, Long>();

    Set<String> set = new HashSet<String>();

    public Map<String, Long> getRows() {
        return rows;
    }

    public void setRows(Map<String, Long> rows) {
        this.rows = rows;
    }

}


