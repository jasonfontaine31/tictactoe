package Bean;

/**
 * Created by Jason on 2/6/16.
 */
public class Row {
    private String name;
    private long wins;

    public Row(String name, long wins){
        this.name = name;
        this.wins = wins;
    }

    public long getWins() {
        return wins;
    }

    public void setWins(long wins) {
        this.wins = wins;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}