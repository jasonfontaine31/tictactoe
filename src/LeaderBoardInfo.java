import Bean.LeaderBoardData;
import Bean.LeaderBoardData.*;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.users.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by Jason on 2/6/16.
 */
public class LeaderBoardInfo extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PreparedQuery pq = getPreparedQuery();
        PrintWriter out = response.getWriter();
        for(Entity result : pq.asIterable()){
            String name = (String) result.getProperty("name");
            long wins = (long) result.getProperty("wins");
            out.println("<tr>");
            out.println("<td>"+name+"</td>");
            out.println("<td>"+wins+"</td>");
            out.println("</tr>");
        }
    }

    public PreparedQuery getPreparedQuery() {
        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

        Query q = new Query("Leaderboard").addSort("wins", Query.SortDirection.DESCENDING);

        return ds.prepare(q);
    }
}
