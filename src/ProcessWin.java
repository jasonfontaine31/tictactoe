import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Jason on 2/5/16.
 */
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.*;


public class ProcessWin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");

        DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
        Key key = new KeyFactory.Builder("Leaderboard",name).getKey();


        Entity leaderboard = getLeaderBoard(ds,name);
//                new Entity("Leaderboard",name);

        leaderboard = (leaderboard == null) ? new Entity("Leaderboard",key) : leaderboard;
        leaderboard.setProperty("name", name);

        long wins = leaderboard.hasProperty("wins") ? (long)leaderboard.getProperty("wins") : 0;

        leaderboard.setProperty("wins", ++wins);

        ds.put(leaderboard);

        //so the logic is follows if the entry is in the table
        // grab it and increment it's wins then put it back
        //redirect back to the leaderboard page


    }

    public Entity getLeaderBoard(DatastoreService ds, String name) {

        Filter keyFilter =  new FilterPredicate("name",FilterOperator.EQUAL,name);

        Query q =  new Query("Leaderboard").setFilter(keyFilter);

        PreparedQuery pq = ds.prepare(q);

            Entity result = pq.asSingleEntity();
        return result;

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
