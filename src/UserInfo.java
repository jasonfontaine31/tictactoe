import Bean.UserBean;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Jason on 2/2/16.
 */
public class UserInfo extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);

    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

//        response.setContentType("text/html;charset=UTF-8");
//        response.setContentType("text/plain");
        HttpSession sc = request.getSession();
        UserService userService = UserServiceFactory.getUserService();
        UserBean bean = (UserBean) sc.getAttribute("userBean");
        if(bean == null)
        {
            bean = new UserBean();
            sc.setAttribute("userBean",bean);
        }

        bean.setName(userService.getCurrentUser().getNickname());
//        bean.setUserName(userService.getCurrentUser().getNickname());

        RequestDispatcher dispatcher = request.getRequestDispatcher("/leaderboard.jsp");
        dispatcher.forward(request,response);



    }
}
