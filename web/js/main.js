/**
 * Created by Jason on 2/2/16.
 */

function Player(name,symbol)
{
    this.name = name;
    this.symbol = symbol;
}
var players = [];
var board = ["","","","","","","","",""];
var isOver = false;
$(document).ready(function(){

    //var player =



    countdown();
    setTimeout(function() {


        var client = new Player($(".container h1.player:first-child").text(), "X");
        var computer = new Player("Comptuer", "O");

        players.push(client);
        players.push(computer);

        var index = Math.round(Math.random());

        alert(players[index].name + " will go first");

        if (index > 0) {
            processComputerTurn(players[1]);
        }
        else
            $('.turn').toggleClass("on");

        $('.tic_table .btn ').click(function () {
            $('.turn').toggleClass("on");
            if ($(this).text() == "" && !isOver) {
                var index = parseInt($(this).attr('id'));
                board[index] = players[0].symbol;
                $(this).text(players[0].symbol);
                if (isOver = isWin(players[0].symbol)) {
                    alert("Player Wins");
                    processWin(players[0].name);
                }
                if (!isOver)
                    processComputerTurn(players[1]);

            }
            console.log(board);


        });

        $('.modal-body .btn').click(function() {
            var thisId = $(this).attr("id");

            switch(thisId)
            {
                case "playagain":
                    $(location).attr('href', "tictactoe.jsp");
                    break;
                case "leaderboard":
                    $(location).attr('href', "leaderboard.jsp");
                    break;
                default:
            }
        });
    },6000);
});

function countdown(){

    var count = 5;

    $('.modal-dialog').css("margin-top",($(window).height()) / 4);
    $('#countDownModal').modal('toggle');
    $('#m-content').text(count);

        var counter=setInterval(function(){
            if(count == 0)
            {
                clearInterval(counter);
                $('#countDownModal').modal('toggle');
                return;

            }
            $('#m-content').text(--count);

        },1000);



}

function processComputerTurn(computer)
{
    if($('.turn').hasClass("on"))
    {
        $('.turn').toggleClass("on");// turns off the signout
    }
    var pos = Math.round(Math.random()*8);
    while($(".tic_table .btn").eq(pos).text() != "")
    {
        pos = Math.round(Math.random()*8);
    }
        $(".tic_table .btn").eq(pos).text(computer.symbol);
        board[pos] = computer.symbol


    if(isOver=isWin(computer.symbol))
    {
        alert("Computer Wins");
        processWin(computer.name);

    }
    if(isBoardFilled() && !isOver)
        processDraw();

    $('.turn').toggleClass("on");// turns the class back on
}
function isBoardFilled()
{
    var isFilled = true;

    for(i = 0; i < board.length;i++){
        if(board[i] == "")
        {
            isFilled = false;
            break;
        }
    }
    return isFilled;

}

function isWin(symbol){
    return isHorizontalWin(symbol) || isVerticalWin(symbol) || isDiagonalWin(symbol);
}

function isHorizontalWin(symbol)
{
    return (board[0] == symbol && board[1]  == symbol && board[2]  == symbol) ||
        (board[3]  == symbol && board[4]  == symbol && board[5]  == symbol)||
        (board[6]  == symbol && board[7]  == symbol && board[8]  == symbol);

}

function isVerticalWin(symbol)
{
    return (board[0]  == symbol && board[3]  == symbol && board[6] == symbol) ||
        (board[1] == symbol && board[4] == symbol && board[7] == symbol) ||
        (board[2]  == symbol && board[5]  == symbol && board[8] == symbol);
}
function isDiagonalWin(symbol)
{
    return (board[0] == symbol && board[4]  == symbol && board[8] == symbol) ||
        (board[2]  == symbol  && board[4]  == symbol && board[6] == symbol);
}
function processWin(player)
{

    $("#winnerModal").modal("toggle");
    $.post("/ProcessWin", {"name" : player});

}
function processDraw()
{
    processWin("draw");
}
