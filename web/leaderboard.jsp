<%--
  Created by IntelliJ IDEA.
  User: Jason
  Date: 2/5/16
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean  id="userBean" class="Bean.UserBean" scope="session" />
<jsp:useBean  id="leaderBean" class="Bean.LeaderBoardData" scope="session" />

<html>
<head>
    <title>LeaderBoard</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="stylesheets/main.css">

    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="container">
    <h1 class="text-center jumbotron">Leaderboard</h1>
        <h2 class="text-center">
            <c:if test="${userBean.name ne null }">
                Hi ${userBean.name}
            </c:if>
            <c:if test="${userBean.name eq null}">
                Please Login before playing
            </c:if>
        </h2>
        <table class="leaderTable table table-hover text-center">
            <thead>
                    <tr>
                        <th>Player</th>
                        <th>Wins</th>

                    </tr>
            </thead>

            <tbody>
                <jsp:include page="/LeaderBoardInfo"/>
            </tbody>
        </table>
        <div class="startupButtons">
            <a href=${userBean.name ne null? "/tictactoe.jsp" : "#"}  class="btn btn-primary btn-lg active center" role="button">Play</a>
            <a href=${userBean.name ne null? "/Logout" : "/Login"} class="btn btn-info  btn-lg active center" role="button">${userBean.name ne null ?"Logout":"Login"}</a>
        </div>
    </div>
</body>
</html>
