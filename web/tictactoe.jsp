<%--
  Created by IntelliJ IDEA.
  User: Jason
  Date: 2/2/16
  Time: 12:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean  id="userBean" class="Bean.UserBean" scope="session" />
<html>
<head>

    <title>Tic Tac Toe</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script src="js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheets/main.css">

    <link href='https://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
</head>
<body>
    <div class="container">
        <h1 class="player text-left">${userBean.name ne null ? userBean.name : "No Name"}</h1>
        <h1 class="player computer text-right">Computer</h1>
    </div>
        <h1 class='turn'>IT'S YOUR TURN</h1>
        <div class="table_conatainer">
            <table class="tic_table">
                <tr >
                    <td><a class="btn" id="0"></a></td>
                    <td><a class="btn" id="1"></a></td>
                    <td><a class="btn" id="2"></a></td>
                </tr>

                <tr>
                    <td><a class="btn" id="3"></a></td>
                    <td><a class="btn" id="4"></a></td>
                    <td><a class="btn" id="5"></a></td>
                </tr>
                <tr>
                    <td><a class="btn" id="6"></a></td>
                    <td><a class="btn" id="7"></a></td>
                    <td><a class="btn" id="8"></a></td>
                </tr>
            </table>
        </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="countDownModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p id="m-content"></p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="winnerModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="startupButtons">
                        <a href="#" class="btn btn-primary btn-lg active center" id="playagain" role="button">PlayAgain</a>
                        <a href="#" class="btn btn-info  btn-lg active center" id="leaderboard" role="button">LeaderBoard</a>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>
